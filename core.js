jQuery.get('home.html').success(function (html) {
  document.getElementById('body').innerHTML = _.template(html)();
  myapp.loadEarthPlugin();
});
google.load('earth', '1');

function loadMyapp ($) {
  console.info('loadMyapp', this, arguments);
  var currentuser;
  var deps = {};
  var router;
  var ge;

  deps.user = $.getJSON('user.json');

  $.when.apply(this, [
    deps.user
  ]).then(function (resolutions) {
    console.info('deps resolved', this, arguments);
    currentuser = new User(resolutions.user);
    (router = new Router(), Backbone.history.start());
  }, function (rejections) {
    console.error('deps rejected', this, arguments);
  });

  deps.earth = $.Deferred();
  function loadEarthPlugin () {
    google.earth.createInstance('earth', function (plugin) {
      console.info('earth.successCallback', this, arguments);
      (ge = plugin).getWindow().setVisibility(true);
      deps.earth.resolve(ge);
    }, function (errorCode) {
      console.error('earth.errorCallback', this, arguments);
      deps.earth.reject(errorCode);
    });
    $.when.apply(this, [
      deps.earth
    ]).then(function (resolutions) {
      console.info('earth resolved', this, arguments);
      // set up gex, etc.
    }, function (rejections) {
      console.error('earth rejected', this, arguments);
    });
  }

  function getCurrentUser () {
    return _.clone(currentuser);
  }

  function getDepsPromise () {
    return $.when.apply(deps, deps);
  }

  return window.myapp = {
    getCurrentUser: getCurrentUser,
    getDepsPromise: getDepsPromise,
    loadEarthPlugin: loadEarthPlugin
  };
}

jQuery(loadMyapp);